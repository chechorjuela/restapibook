﻿using BooksApiWeb.Domain.Contracts.Request;
using BooksApiWeb.Domain.Contracts.Response;
using BooksApiWeb.Domain.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BooksApiWeb.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorController : ControllerBase
    {
        private readonly IAuthorService _authorService;
        public AuthorController(IAuthorService authorService)
        {
            this._authorService = authorService;
        }
        [HttpGet]
        public async Task<ResponseContract<List<AuthorResponseContract>>> index()
        {
            ResponseContract<List<AuthorResponseContract>> response = new ResponseContract<List<AuthorResponseContract>>();
            try
            {
                response.Data = _authorService.getAll();
            }
            catch (Exception ex)
            {
                response.Header.Code = HttpCodes.InternalServerError;
                response.Header.Message = ex.Message;
            }
            return response;
        }
        [HttpPost]
        public async Task<ResponseContract<AuthorResponseContract>> create([FromBody]AuthorRequestContract authorRequest)
        {
            ResponseContract<AuthorResponseContract> response = new ResponseContract<AuthorResponseContract>();
            try
            {
                response.Data =await _authorService.create(authorRequest);
            }
            catch (Exception ex)
            {
                response.Header.Code = HttpCodes.InternalServerError;
                response.Header.Message = ex.Message;
            }
            return response;
        }
        [HttpPut("{id}")]
        public async Task<ResponseContract<AuthorResponseContract>> update(int id, [FromBody] AuthorRequestContract authorRequest)
        {
            ResponseContract<AuthorResponseContract> response = new ResponseContract<AuthorResponseContract>();
            try
            {
                response.Data = await _authorService.update(id, authorRequest);
            }
            catch (Exception ex)
            {
                response.Header.Code = HttpCodes.InternalServerError;
                response.Header.Message = ex.Message;
            }
            return response;
        }
        [HttpDelete("{id}")]
        public async Task<ResponseContract<Boolean>> delete(int id)
        {
            ResponseContract<Boolean> response = new ResponseContract<Boolean>();
            try
            {
                response.Data = await _authorService.delete(id);
            }
            catch (Exception ex)
            {
                response.Header.Code = HttpCodes.InternalServerError;
                response.Header.Message = ex.Message;
            }
            return response;
        }
    }
}
