﻿using BooksApiWeb.Domain;
using BooksApiWeb.Domain.Contracts;
using BooksApiWeb.Domain.Contracts.Request;
using BooksApiWeb.Domain.Contracts.Response;
using BooksApiWeb.Domain.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksApiWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        public AuthController(IAuthService authService) {
            _authService = authService;
        }
        [HttpPost]
        [Route("signup")]
        public async Task<ResponseContract<SignUpResponseContract>> signup([FromBody] SignUpRequestContract signContract)
        {
            ResponseContract<SignUpResponseContract> response = new ResponseContract<SignUpResponseContract>();
            try {
                response.Data = await _authService.SignUp(signContract);
            } catch (Exception exp) {
                response.Header.Code = HttpCodes.InternalServerError;
                response.Header.Message = exp.Message;
            }
            return response;
        }

        [HttpPost]
        [Route("signin")]
        public async Task<ResponseContract<SignInResponseContract>> signin([FromBody] SignInRequestContract signContract)
        {
            ResponseContract<SignInResponseContract> response = new ResponseContract<SignInResponseContract>();
            try
            {
                response.Data = await _authService.SignIn(signContract);
            }
            catch (Exception exp)
            {
                response.Header.Code = HttpCodes.InternalServerError;
                response.Header.Message = exp.Message;
            }
            return response;
        }

        [HttpGet]
        public async Task<ResponseContract<SignInResponseContract>> forgotpassword(SignInRequestContract signContract)
        {
            ResponseContract<SignInResponseContract> response = new ResponseContract<SignInResponseContract>();
            try
            {

            }
            catch (Exception exp)
            {

            }
            return response;
        }

    }
}
