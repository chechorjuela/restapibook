﻿using BooksApiWeb.Domain.Contracts.Request;
using BooksApiWeb.Domain.Contracts.Response;
using BooksApiWeb.Domain.Services.Interfaces;
using BooksApiWeb.Model.Models;
using BooksApiWeb.Repository.Core.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksApiWeb.Controllers
{
    [Authorize]
    [Route("api/book")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly IBookService _bookService;

        public BookController(IBookService bookService)
        {
            this._bookService = bookService;
        }

        [HttpGet]
        public ResponseContract<List<BookResponseContract>> index()
        {
            ResponseContract<List<BookResponseContract>> response = new ResponseContract<List<BookResponseContract>>();
            try
            {
                response.Data = this._bookService.getAllBook();
            }
            catch (Exception ex)
            {
                response.Header.Code = HttpCodes.InternalServerError;
                response.Header.Message = ex.Message;
            }
            return response;
        }
        [HttpPost]
        public async Task<ResponseContract<BookResponseContract>> create([FromBody] BookRequestContract book)
        {
            ResponseContract<BookResponseContract> response = new ResponseContract<BookResponseContract>();
            try
            {
                response.Data = await this._bookService.create(book);
            }
            catch(Exception ex)
            {
                response.Header.Code = HttpCodes.InternalServerError;
                response.Header.Message = ex.Message;
            }
            return response;
        }
        [HttpPut("{id}")]
        public async Task<ResponseContract<BookResponseContract>> update(int id, [FromBody]BookRequestContract book)
        {
            ResponseContract<BookResponseContract> response = new ResponseContract<BookResponseContract>();
            try
            {
                response.Data = await this._bookService.update(id,book);
            }
            catch (Exception ex)
            {
                response.Header.Code = HttpCodes.InternalServerError;
                response.Header.Message = ex.Message;
            }
            return response;
        }
        [HttpDelete("{id}")]
        public async Task<ResponseContract<Boolean>> delete(int id)
        {
            ResponseContract<Boolean> response = new ResponseContract<Boolean>();
            try
            {
                response.Data = await this._bookService.delete(id);
            }
            catch (Exception ex)
            {
                response.Header.Code = HttpCodes.InternalServerError;
                response.Header.Message = ex.Message;
            }
            return response;
        }
    }
}
