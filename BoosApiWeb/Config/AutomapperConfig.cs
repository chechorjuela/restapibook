﻿namespace BooksApiWeb.Config
{
    public class AutomapperConfig
    {
        public static void CreateMaps() { 
            BooksApiWeb.Domain.Config.AutomapperConfig.CreateMaps();
        }
    }
}
