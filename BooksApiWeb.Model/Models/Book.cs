﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksApiWeb.Model.Models
{
    public class Book
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int AuthorId { get; set; }
        [Required]
        public string Editorial { get; set; }

        [Required]
        public int Status { get; set; }

        [Required]
        public DateTime DatePublish { get; set; }

        [Required]
        public DateTime CreateAt { get; set; }
        
        public DateTime? UpdateAt { get; set; }
        public DateTime? DeleteAt { get; set; }

    }
}
