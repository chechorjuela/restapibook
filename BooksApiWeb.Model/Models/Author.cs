﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksApiWeb.Model.Models
{
    public class Author
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Fullname { get; set; }

        [Required]
        public DateTime Birthdate { get; set; }
        
        [Required]
        public DateTime Create_at { get; set; }

        public DateTime? Update_at { get; set; }

    }
}
