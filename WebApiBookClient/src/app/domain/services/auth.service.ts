import { Auth } from './../Model/auth';
import {Injectable} from '@angular/core';
import {AuthRepository} from '../../intrastrucutre/auth.repository';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import {Router} from "@angular/router";

export abstract class IAuthService {

  abstract signIn(SignIn: any): Promise<any>;

  abstract signUp(SignUp: any): Promise<any>;

}



@Injectable()
export class AuthService implements IAuthService {
  private currentUserSubject: BehaviorSubject<Auth>;
  public currentUser: Observable<Auth>;

  constructor(public AuthRepo: AuthRepository,private router:Router) {
    this.currentUserSubject = new BehaviorSubject<Auth>(JSON.parse(<string>localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  signIn(SignIn: any): Promise<any> {
    return this.AuthRepo.Sigin(SignIn).then((response: any) => {
      if(typeof response.data.token!='undefined'){
        localStorage.setItem('currentUser', JSON.stringify(response.data));
        this.currentUserSubject.next(response);
        this.router.navigate(['book'])
        return response;

      }

      return response;
    });
  }

  signUp(SignUp: any): Promise<any> {
    return this.AuthRepo.SignUp(SignUp).then((response: any) => {
      this.router.navigate(['signin'])
      return response;
    });
  }

}
