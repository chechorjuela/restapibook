import {Injectable} from '@angular/core';
import {AuthorRepository} from '../../intrastrucutre/author.repository';

@Injectable()
export abstract class IAuthorService {
  abstract getAuthorAll(): Promise<any>;
  abstract saveAuthor(Author: any): Promise<any>;
  abstract updateAuthor(Author: any): Promise<any>;
  abstract deleteAuthor(Author: any): Promise<any>;
}


@Injectable()
export class AuthorService implements IAuthorService {
  constructor(public AuthorRepo: AuthorRepository) {
  }

  getAuthorAll(): Promise<any> {
    return this.AuthorRepo.GetAuthorAll().then((data: any) => {
      return data;
    });
  }

  saveAuthor(Author: any): Promise<any> {
    return this.AuthorRepo.CreateAuthor(Author).then((response: any) => {
      return response;
    });
  }

  deleteAuthor(Author: any): Promise<any> {
    return this.AuthorRepo.deleteAuthor(Author).then((response: any) => {
      return response;
    });
  }

  updateAuthor(Author: any): Promise<any> {
    return this.AuthorRepo.UpdateAuthor(Author).then((response: any) => {
      return response;
    });
  }
}
