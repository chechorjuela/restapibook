import {Injectable} from '@angular/core';
import {BookRepository} from '../../intrastrucutre/book.repository';
@Injectable()
export abstract class IBookService {
  abstract getBookAll(): Promise<any>;

  abstract saveBook(Book: any): Promise<any>;

  abstract updateBook(Book: any): Promise<any>;

  abstract deleteBook(Book: any): Promise<any>;
}


@Injectable()
export class BookService implements IBookService {
  constructor(public BookRepo: BookRepository) {
  }

  getBookAll(): Promise<any> {
    return this.BookRepo.GetBookAll().then(data => {
      return data;
    });
  }

  saveBook(Book: any): Promise<any> {
    return this.BookRepo.CreateBook(Book).then(response => {
      return response;
    });
  }

  deleteBook(Book: any): Promise<any> {
    return this.BookRepo.deleteBook(Book).then(response => {
      return response;
    });
  }

  updateBook(Book: any): Promise<any> {
    return this.BookRepo.UpdateBook(Book).then(response => {
      return response;
    });
  }
}
