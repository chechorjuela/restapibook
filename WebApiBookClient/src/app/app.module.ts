import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/appComponent/app.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDialogModule} from '@angular/material/dialog';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  MatNativeDateModule,
  MatRippleModule
} from '@angular/material/core';
import {MatCardModule} from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatListModule} from '@angular/material/list';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSelectModule} from '@angular/material/select';
import {BookComponent} from './components/bookComponent/book.component';
import {AuthorComponent} from './components/authorComponent/author.component';
import {SignInComponent} from './components/authComponent/signInComponent/signin.component';
import {SignupComponent} from './components/authComponent/signUpComponent/signup.component';
import {AppSettings} from './setting/app.setting';
import {DatePipe} from '@angular/common';
import {
  BookService,
  IBookService
} from './domain/services/book.service';
import {
  BookRepository,
  IBookRepository
} from './intrastrucutre/book.repository';
import {
  AuthorService,
  IAuthorService
} from './domain/services/author.service';
import {
  AuthorRepository,
  IAuthorRepository
} from './intrastrucutre/author.repository';
import {
  AuthService,
  IAuthService
} from './domain/services/auth.service';
import {
  AuthRepository,
  IAuthRepository
} from './intrastrucutre/auth.repository';
import {FlexLayoutModule} from '@angular/flex-layout';
import {CdkTreeModule} from '@angular/cdk/tree';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDividerModule} from '@angular/material/divider';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTreeModule} from '@angular/material/tree';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatRadioModule} from '@angular/material/radio';
import {PortalModule} from '@angular/cdk/portal';
import {MatBadgeModule} from '@angular/material/badge';
import {OverlayModule} from '@angular/cdk/overlay';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatSortModule} from '@angular/material/sort';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {AuthGuard} from "./helpers/Guard/auth.guard";
import {BookModalComponet} from "./components/HelpersComponent/Modal/Book/bookModal.component";
import {ConfirmdialogComponent} from "./components/HelpersComponent/Modal/Confirm/confirmDialog.component";
import {AuthorModalComponet} from "./components/HelpersComponent/Modal/Author/authorModal.component";
import {ConfirmDialogAuthorComponent} from "./components/HelpersComponent/Modal/ConfirmAuthor/confirmDialogAuthor.component";

@NgModule({
  declarations: [
    AppComponent,
    BookComponent,
    AuthorComponent,
    SignInComponent,
    SignupComponent,
    BookModalComponet,
    ConfirmdialogComponent,
    AuthorModalComponet,
    ConfirmDialogAuthorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatCardModule,
    ReactiveFormsModule,
    FormsModule,
    MatToolbarModule,
    MatListModule,
    MatSidenavModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    FlexLayoutModule,
    CdkTreeModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatPaginatorModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSortModule,
    MatTableModule,
    MatToolbarModule,
    MatFormFieldModule,
    OverlayModule,
    PortalModule,
    MatDatepickerModule,
    MatTooltipModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    AppSettings,
    DatePipe,
    {provide: IBookService, useClass: BookService},
    {provide: IBookRepository, useClass: BookRepository},
    {provide: IAuthorService, useClass: AuthorService},
    {provide: IAuthorRepository, useClass: AuthorRepository},
    {provide: IAuthService, useClass: AuthService},
    {provide: IAuthRepository, useClass: AuthRepository},
    AuthGuard
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
