import {Component} from '@angular/core';
import {AuthService} from '../../../domain/services/auth.service';
import {AuthRepository} from '../../../intrastrucutre/auth.repository';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css'],
  providers: [AuthService, AuthRepository]
})
export class SignInComponent {
  // @ts-ignore
  loginForm: FormGroup;
  emailRegx = /^(([^<>+()\[\]\\.,;:\s@"-#$%&=]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/;

  constructor(private authService: AuthService, private formBuilder: FormBuilder) {

  }
  ngOnInit(){
    this.loginForm = this.formBuilder.group({
      useremail: [null, [Validators.required, Validators.pattern(this.emailRegx)]],
      password: [null, Validators.required]
    });
  }
  onAuth(): void {
    const forms = this.loginForm?.value;
    this.authService.signIn(forms).then(response => {
      console.info(response);
    });
  }
}
