import { Component } from '@angular/core';
import {AuthorService} from "../../../domain/services/author.service";
import {AuthorRepository} from "../../../intrastrucutre/author.repository";
import {AuthRepository} from "../../../intrastrucutre/auth.repository";
import {AuthService} from "../../../domain/services/auth.service";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  providers: [AuthService, AuthRepository],
  selector: 'app-root',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {
  public formGroup: FormGroup = new FormGroup({
    Firstname: new FormControl(''),
    Lastname: new FormControl(''),
    UserEmail: new FormControl(''),
    UserName: new FormControl(''),
    Password: new FormControl(''),
  });

  constructor(private authService:AuthService) {
  }
  onSubmit(): void {
    console.info(this.formGroup.value);
      this.authService.signUp(this.formGroup.value).then(response => {
      });

  }
}
