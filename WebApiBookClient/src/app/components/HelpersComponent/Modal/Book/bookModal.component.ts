import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DatePipe} from '@angular/common';
import {BookService, IBookService} from "../../../../domain/services/book.service";
import {BookRepository,IBookRepository} from "../../../../intrastrucutre/book.repository";
import {AuthorRepository} from "../../../../intrastrucutre/author.repository";
import {AuthorService} from "../../../../domain/services/author.service";

export interface DialogData {
  id: number;
  name: string;
  editorial: string;
  authorId: number;
  datePublish: Date;

}

@Component({
  providers: [ BookRepository, BookService, AuthorRepository, AuthorService ],
  templateUrl: './bookModal.component.html',
  styleUrls: ['./bookModal.css'],
})
export class BookModalComponet implements OnInit {
  public authorsList: any;
  public formGroup: FormGroup = new FormGroup({
    name: new FormControl(''),
    datePublish: new FormControl(''),
    authorId: new FormControl(''),
    editorial: new FormControl(''),
  });

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<BookModalComponet>,
    private bookService: BookService,
    private authorService:AuthorService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public datepipe: DatePipe) {

  }

  ngOnInit(): void {
    this.authorService.getAuthorAll().then(response=>{
      this.authorsList = response.data;
    })
    this.buildForm();
  }

  private buildForm(): void {
    if (this.data != null) {
      this.formGroup = this.formBuilder.group({
        id: this.data.id != null ? this.data.id : '',
        name: this.data.name != null ? this.data.name : '',
        editorial: this.data.editorial != null ? this.data.editorial : '',
        authorId: this.data.authorId != null ? this.data.authorId : '',
        datePublish: this.data.datePublish != null ? this.data.datePublish : '',
      });
    }

  }

  onSubmit(): void {
    if (this.formGroup.value.id <= 0) {
      this.formGroup.value.datePublish = this.datepipe.transform(this.formGroup.value.datePublish, 'yyyy-MM-dd');
      this.bookService.saveBook(this.formGroup.value).then(response => {
        this.dialogRef.close();
      });
    } else {
      this.formGroup.value.datePublish = this.datepipe.transform(this.formGroup.value.datePublish, 'yyyy-MM-dd');
      this.bookService.updateBook(this.formGroup.value).then(response => {
        this.dialogRef.close();
      });
    }
  }
}
