import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DatePipe} from '@angular/common';
import {BookService} from "../../../../domain/services/book.service";
import {BookRepository} from "../../../../intrastrucutre/book.repository";
import {AuthService} from "../../../../domain/services/auth.service";
import {AuthorRepository} from "../../../../intrastrucutre/author.repository";
import {AuthorService} from "../../../../domain/services/author.service";

export interface DialogData {
  id: number;
  fullname: string;
  birthdate: string;

}

@Component({
  selector: 'app-bookModal',
  templateUrl: './authorModal.component.html',
  styleUrls: ['./authorModal.css'],
  providers: [AuthorService, AuthorRepository]
})
export class AuthorModalComponet implements OnInit {
  public formGroup: FormGroup = new FormGroup({
    fullname: new FormControl(''),
    birthdate: new FormControl(''),
  });

  constructor(private formBuilder: FormBuilder, public dialogRef: MatDialogRef<AuthorModalComponet>,
              private authService: AuthorService, @Inject(MAT_DIALOG_DATA) public data: DialogData, public datepipe: DatePipe) {
  }

  ngOnInit(): void {
    this.buildForm();
  }

  private buildForm(): void {
    if (this.data != null) {
      this.formGroup = this.formBuilder.group({
        id: this.data.id != null ? this.data.id : '',
        fullname: this.data.fullname != null ? this.data.fullname : '',
        birthdate: this.data.birthdate != null ? this.data.birthdate : '',
      });
    }

  }

  onSubmit(): void {
    if (this.formGroup.value.id <= 0) {
      this.formGroup.value.birthdate = this.datepipe.transform(this.formGroup.value.birthdate, 'yyyy-MM-dd');
      this.authService.saveAuthor(this.formGroup.value).then(response => {
        this.dialogRef.close();
      });
    } else {
      this.formGroup.value.birthdate = this.datepipe.transform(this.formGroup.value.birthdate, 'yyyy-MM-dd');
      this.authService.updateAuthor(this.formGroup.value).then(response => {
        this.dialogRef.close();
      });
    }
  }
}
