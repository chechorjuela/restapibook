import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {BookService} from "../../../../domain/services/book.service";
import {BookRepository} from "../../../../intrastrucutre/book.repository";
import {DialogData} from "../Book/bookModal.component";
import {AuthorService} from "../../../../domain/services/author.service";
import {AuthorRepository} from "../../../../intrastrucutre/author.repository";

@Component({
  selector: 'app-confirmdialog',
  templateUrl: './confirmdialogAuthor.component.html',
  styleUrls: ['./confirmdialogAuthor.component.css'],
  providers: [AuthorService, AuthorRepository]
})
export class ConfirmDialogAuthorComponent implements OnInit {

  constructor(private authService: AuthorService, public dialogRef: MatDialogRef<ConfirmDialogAuthorComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  ngOnInit(): void {
  }

  onDelete(): void {
    this.authService.deleteAuthor(this.data).then(response => {
      if(response.data){
        this.dialogRef.close();
      }
    });
  }
}
