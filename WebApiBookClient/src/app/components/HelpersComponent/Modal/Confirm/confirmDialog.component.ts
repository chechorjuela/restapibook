import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {BookService} from "../../../../domain/services/book.service";
import {BookRepository} from "../../../../intrastrucutre/book.repository";
import {DialogData} from "../Book/bookModal.component";

@Component({
  selector: 'app-confirmdialog',
  templateUrl: './confirmdialog.component.html',
  styleUrls: ['./confirmdialog.component.css'],
  providers: [BookService, BookRepository]
})
export class ConfirmdialogComponent implements OnInit {

  constructor(private bookService: BookService, public dialogRef: MatDialogRef<ConfirmdialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  ngOnInit(): void {
  }

  onDelete(): void {
    this.bookService.deleteBook(this.data).then(response => {
      if(response.data){
        this.dialogRef.close();
      }
    });
  }
}
