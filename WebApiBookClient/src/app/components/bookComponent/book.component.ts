import { Component } from '@angular/core';
import {BookService} from "../../domain/services/book.service";
import {BookRepository} from "../../intrastrucutre/book.repository";
import {MatDialog} from "@angular/material/dialog";
import {BookModalComponet} from "../HelpersComponent/Modal/Book/bookModal.component";
import {ConfirmdialogComponent} from "../HelpersComponent/Modal/Confirm/confirmDialog.component";
import {AuthorRepository} from "../../intrastrucutre/author.repository";
import {AuthorService} from "../../domain/services/author.service";
import {AuthService} from "../../domain/services/auth.service";

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.sass'],
  providers: [BookService, BookRepository,AuthorRepository,AuthorService]
})
export class BookComponent {
  public booksList: any;
  displayedColumns: string[] = ['id', 'name', 'editorial', 'datePublish', 'available', 'Actions'];

  constructor(public bookService:BookService,private authorService:AuthorService,private dialog: MatDialog) {

  }
  ngOnInit(){
    this.loadingPage();
  }

  changeDialog(): void {
    const dialogConfig = this.dialog.open(BookModalComponet, {
      data: {
        id: 0,
        date_expiration: '',
        name: '',
        amount: '',
        available: '',
      },
    });
    dialogConfig.afterClosed().subscribe(result => {
      this.loadingPage();
    });
  }
  loadingPage(): void {
    this.bookService.getBookAll().then(response=>{
      this.booksList = response.data
    })
  }
  onEdit(book: any): void {
    const dialogConfig = this.dialog.open(BookModalComponet, {
      data: book,
    });
    dialogConfig.afterClosed().subscribe(result => {
      this.loadingPage();
    });

  }


  onDelete(book: any): void {
    const deleteDialog = this.dialog.open(ConfirmdialogComponent, {
      data: book
    });
    deleteDialog.afterClosed().subscribe(result => {
      this.loadingPage();
    });
  }
}
