import { Component } from '@angular/core';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent{
  title = 'WebApiBookClient';
  isAuthenticated: boolean=false;
  constructor() {
    const currentUser = JSON.parse(<string>localStorage.getItem('currentUser'));
    if(currentUser!=null){
      this.isAuthenticated = true;
    }
  }

}
