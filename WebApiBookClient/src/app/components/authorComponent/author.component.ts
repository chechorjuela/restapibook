import { Component } from '@angular/core';
import {BookService} from "../../domain/services/book.service";
import {BookRepository} from "../../intrastrucutre/book.repository";
import {MatDialog} from "@angular/material/dialog";
import {AuthorService} from "../../domain/services/author.service";
import {AuthorRepository} from "../../intrastrucutre/author.repository";
import {BookModalComponet} from "../HelpersComponent/Modal/Book/bookModal.component";
import {ConfirmdialogComponent} from "../HelpersComponent/Modal/Confirm/confirmDialog.component";
import {AuthorModalComponet} from "../HelpersComponent/Modal/Author/authorModal.component";
import {ConfirmDialogAuthorComponent} from "../HelpersComponent/Modal/ConfirmAuthor/confirmDialogAuthor.component";

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.sass'],
  providers: [AuthorService, AuthorRepository]
})
export class AuthorComponent {
  public authorsList: any;
  displayedColumns: string[] = ['id', 'fullname', 'birthdate','Actions'];
  constructor(public authorService:AuthorService,private dialog: MatDialog) {

  }
  ngOnInit(){
    this.loadingPage();
  }
  loadingPage(): void {
    this.authorService.getAuthorAll().then(response=>{
      this.authorsList = response.data;
      console.info(this.authorsList);
    })
  }


  changeDialog(): void {
    const dialogConfig = this.dialog.open(AuthorModalComponet, {
      data: {
        id: 0,
        date_expiration: '',
        name: '',
        amount: '',
        available: '',
      },
    });
    dialogConfig.afterClosed().subscribe(result => {
      this.loadingPage();
    });
  }


  onEdit(book: any): void {
    const dialogConfig = this.dialog.open(AuthorModalComponet, {
      data: book,
    });
    dialogConfig.afterClosed().subscribe(result => {
      this.loadingPage();
    });

  }


  onDelete(book: any): void {
    const deleteDialog = this.dialog.open(ConfirmDialogAuthorComponent, {
      data: book
    });
    deleteDialog.afterClosed().subscribe(result => {
      this.loadingPage();
    });
  }
}
