import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AppSettings} from '../setting/app.setting';

@Injectable()
export abstract class IAuthRepository {
  abstract Sigin(SignIn: any): Promise<any>;
  abstract SignUp(Author: any): Promise<any>;
}

@Injectable()
export class AuthRepository implements IAuthRepository {
  private constants: AppSettings;
  private urlAuthSignIn: string;
  private urlAuthSignUp: string;
  constructor(private http: HttpClient) {
    this.constants = new AppSettings();
    this.urlAuthSignIn = `${this.constants.SiteUrl}/auth/signin`;
    this.urlAuthSignUp = `${this.constants.SiteUrl}/auth/signup`;
  }



  Sigin(SignIn: any): Promise<any> {
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});

    return this.http.post(this.urlAuthSignIn, SignIn, {headers}).toPromise().then(response => {
      return response;
    });
  }

  SignUp(Author: any): Promise<any> {
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    return this.http.post(`${this.urlAuthSignUp}`, Author,{headers}).toPromise().then(response => {
      return response;
    });
  }

}
