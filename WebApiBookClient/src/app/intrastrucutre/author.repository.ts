import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AppSettings} from '../setting/app.setting';

@Injectable()
export abstract class IAuthorRepository {
  abstract GetAuthorAll(): Promise<any>;

  abstract CreateAuthor(Author: any): Promise<any>;

  abstract UpdateAuthor(Author: any): Promise<any>;

  abstract deleteAuthor(Author: any): Promise<any>;
}

@Injectable()
export class AuthorRepository implements IAuthorRepository {
  private constants: AppSettings;
  private urlAuthor: string;

  constructor(private http: HttpClient) {
    this.constants = new AppSettings();
    this.urlAuthor = `${this.constants.SiteUrl}/Author`;
  }

  GetAuthorAll(): Promise<any> {
    const authToken = JSON.parse(<string>localStorage.getItem('currentUser'));
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8','Authorization':`Bearer ${authToken.token}`});
    return this.http.get(this.urlAuthor,{headers}).toPromise().then(response => {
      return response;
    });
  }

  CreateAuthor(Author: any): Promise<any> {
    const authToken = JSON.parse(<string>localStorage.getItem('currentUser'));
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8','Authorization':`Bearer ${authToken.token}`});
    return this.http.post(this.urlAuthor, Author,{headers}).toPromise().then(response => {
      return response;
    });
  }

  UpdateAuthor(Author: any): Promise<any> {
    const authToken = JSON.parse(<string>localStorage.getItem('currentUser'));
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8','Authorization':`Bearer ${authToken.token}`});
    return this.http.put(`${this.urlAuthor}/${Author.id}`, Author,{headers}).toPromise().then(response => {
      return response;
    });
  }

  deleteAuthor(Author: any): Promise<any> {
    const authToken = JSON.parse(<string>localStorage.getItem('currentUser'));
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8','Authorization':`Bearer ${authToken.token}`});
    return this.http.delete(`${this.urlAuthor}/${Author.id}`,{headers}).toPromise().then(response => {
      return response;
    });
  }

}
