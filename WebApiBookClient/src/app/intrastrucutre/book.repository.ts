import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AppSettings} from '../setting/app.setting';

@Injectable()
export abstract class IBookRepository {
  abstract GetBookAll(): Promise<any>;

  abstract CreateBook(Book: any): Promise<any>;

  abstract UpdateBook(Book: any): Promise<any>;

  abstract deleteBook(Book: any): Promise<any>;
}

@Injectable()
export class BookRepository implements IBookRepository {
  private constants: AppSettings;
  private urlBook: string;

  constructor(private http: HttpClient) {
    this.constants = new AppSettings();
    this.urlBook = `${this.constants.SiteUrl}/Book`;
  }

  GetBookAll(): Promise<any> {
    const authToken = JSON.parse(<string>localStorage.getItem('currentUser'));
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8','Authorization':`Bearer ${authToken.token}`});
    return this.http.get(this.urlBook,{headers}).toPromise().then(response => {
      return response;
    });
  }

  CreateBook(Book: any): Promise<any> {
    const authToken = JSON.parse(<string>localStorage.getItem('currentUser'));
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8','Authorization':`Bearer ${authToken.token}`});
    Book.Status = 1;
    return this.http.post(this.urlBook, Book,{headers}).toPromise().then(response => {
      return response;
    });
  }

  UpdateBook(Book: any): Promise<any> {
    const authToken = JSON.parse(<string>localStorage.getItem('currentUser'));
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8','Authorization':`Bearer ${authToken.token}`});
    Book.Status = 1;
    return this.http.put(`${this.urlBook}/${Book.id}`, Book,{headers}).toPromise().then(response => {
      return response;
    });
  }

  deleteBook(Book: any): Promise<any> {
    const authToken = JSON.parse(<string>localStorage.getItem('currentUser'));
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8','Authorization':`Bearer ${authToken.token}`});
    return this.http.delete(`${this.urlBook}/${Book.id}`,{headers}).toPromise().then(response => {
      return response;
    });
  }

}
