declare var process: any;

export class AppSettings {
  public SiteUrl: string;

  constructor() {
    this.SiteUrl = 'https://localhost:44369/api';
  }
}
