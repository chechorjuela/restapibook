import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BookComponent} from './components/bookComponent/book.component';
import {SignInComponent} from './components/authComponent/signInComponent/signin.component';
import {SignupComponent} from './components/authComponent/signUpComponent/signup.component';
import {AuthGuard} from "./helpers/Guard/auth.guard";
import {AuthorComponent} from "./components/authorComponent/author.component";

const routes: Routes = [
  {path: 'signin', component: SignInComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'book', component: BookComponent, canActivate: [AuthGuard]},
  {path: 'author', component: AuthorComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
