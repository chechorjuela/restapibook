﻿using BooksApiWeb.Model.Models;
using BooksApiWeb.Repository.Base;
using BooksApiWeb.Repository.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksApiWeb.Repository.Core.Repositories
{
    public class AuthorRepository : Repository<Author>, IAuthorRepository
    {
        private DataContext _context;
        public AuthorRepository(DataContext context):base(context) {
            _context = context;
        }
        public Author Get(int Id)
        {
            return _context.author.Find(Id);
        }
        public List<Author> GetAll()
        {
            return _context.author.ToList();
        }
        public Author Create(Author author)
        {
            _context.author.Add(author);
            _context.SaveChanges();
            return author;

        }
        public Author UpdateAuthor(int id, Author author)
        {
            _context.author.Update(author);
            _context.SaveChanges();
            return author;
        }
        public Boolean delete(Author author)
        {
            Boolean response = false;
            var deletbook = _context.author.Remove(author);
            _context.SaveChanges();
            if (deletbook != null)
            {
                response = true;
            }
            return response;
        }
    }
}
