﻿using BooksApiWeb.Model.Models;
using BooksApiWeb.Repository.Base;
using System.Collections.Generic;
using System.Linq;

namespace BooksApiWeb.Repository.Core.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        private DataContext _context;

        public UserRepository(DataContext context): base(context) {
            _context = context;
        }
        public List<User> getAll() {
            return _context.user.ToList();
        }

       
    }
}
