﻿using BooksApiWeb.Model.Models;
using BooksApiWeb.Repository.Base;
using BooksApiWeb.Repository.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksApiWeb.Repository.Core.Repositories
{
    public class BookRepository : Repository<Book>, IBookRepository
    {
        private DataContext _context;
        public BookRepository(DataContext context) : base(context) {
            _context = context;
        }
        public Book Get(int Id) {
            return _context.book.Find(Id);
        }
        public List<Book> GetAll()
        {
            return _context.book.ToList();
        }
        public Book Create(Book book) {
            _context.book.Add(book);
            _context.SaveChanges();
            return book;

        }
        public Book UpdateBook(int id,Book book) {
          
            _context.book.Update(book);
            _context.SaveChanges();
            return book;
        }
        public Boolean delete(Book book)
        {
            Boolean response = false;
            var deletbook = _context.book.Remove(book);
            _context.SaveChanges();
            if (deletbook!=null) {
                response = true;
            }
            return response;
        }
    }
}
