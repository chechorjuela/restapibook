﻿using BooksApiWeb.Model.Models;
using BooksApiWeb.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksApiWeb.Repository.Core
{
    public interface IUserRepository : IRepository<User>
    {

    }
}
