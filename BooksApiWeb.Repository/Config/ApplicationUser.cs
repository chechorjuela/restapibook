﻿using Microsoft.AspNetCore.Identity;

namespace BooksApiWeb.Repository.Config
{
    public class ApplicationUser : IdentityUser
    {

    }
}
