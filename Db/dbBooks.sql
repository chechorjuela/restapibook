USE [master]
GO
/****** Object:  Database [booksdb]    Script Date: 4/24/2021 10:02:01 PM ******/
CREATE DATABASE [booksdb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'booksdb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\booksdb.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'booksdb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\booksdb_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [booksdb] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [booksdb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [booksdb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [booksdb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [booksdb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [booksdb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [booksdb] SET ARITHABORT OFF 
GO
ALTER DATABASE [booksdb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [booksdb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [booksdb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [booksdb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [booksdb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [booksdb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [booksdb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [booksdb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [booksdb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [booksdb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [booksdb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [booksdb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [booksdb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [booksdb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [booksdb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [booksdb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [booksdb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [booksdb] SET RECOVERY FULL 
GO
ALTER DATABASE [booksdb] SET  MULTI_USER 
GO
ALTER DATABASE [booksdb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [booksdb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [booksdb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [booksdb] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [booksdb] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [booksdb] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'booksdb', N'ON'
GO
ALTER DATABASE [booksdb] SET QUERY_STORE = OFF
GO
USE [booksdb]
GO
/****** Object:  Table [dbo].[Author]    Script Date: 4/24/2021 10:02:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Author](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Fullname] [varchar](50) NULL,
	[Birthdate] [date] NULL,
	[Create_at] [date] NULL,
	[Update_at] [date] NULL,
 CONSTRAINT [PK_Author] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Book]    Script Date: 4/24/2021 10:02:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Book](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Editorial] [varchar](50) NULL,
	[Name] [varchar](50) NULL,
	[Status] [int] NULL,
	[AuthorId] [int] NULL,
	[DatePublish] [date] NULL,
	[UpdateAt] [date] NULL,
	[CreateAt] [date] NULL,
	[DeleteAt] [date] NULL,
 CONSTRAINT [PK_Book] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 4/24/2021 10:02:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Firstname] [varchar](50) NULL,
	[Lastname] [varchar](50) NULL,
	[Password] [varchar](max) NULL,
	[Email] [varchar](150) NULL,
	[UserName] [varchar](50) NULL,
	[SecurityStamp] [varchar](150) NULL,
	[EmailConfirmed] [bit] NULL,
	[UserEmail] [varchar](50) NULL,
	[TwoFactorEnabled] [bit] NULL,
	[PhoneNumberConfirmed] [bit] NULL,
	[PhoneNumber] [varchar](50) NULL,
	[NormalizedEmail] [varchar](150) NULL,
	[AccessFailedCount] [int] NULL,
	[ConcurrencyStamp] [varchar](max) NULL,
	[LockoutEnabled] [bit] NULL,
	[NormalizedUserName] [varchar](50) NULL,
	[PasswordHash] [varchar](max) NULL,
	[LockoutEnd] [date] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Author] ON 

INSERT [dbo].[Author] ([Id], [Fullname], [Birthdate], [Create_at], [Update_at]) VALUES (1, N'name 1', CAST(N'2021-04-02' AS Date), CAST(N'0001-01-01' AS Date), CAST(N'2021-04-24' AS Date))
INSERT [dbo].[Author] ([Id], [Fullname], [Birthdate], [Create_at], [Update_at]) VALUES (2, N'name 2', CAST(N'2021-04-07' AS Date), CAST(N'0001-01-01' AS Date), NULL)
SET IDENTITY_INSERT [dbo].[Author] OFF
GO
SET IDENTITY_INSERT [dbo].[Book] ON 

INSERT [dbo].[Book] ([Id], [Editorial], [Name], [Status], [AuthorId], [DatePublish], [UpdateAt], [CreateAt], [DeleteAt]) VALUES (2, N'normal', N'name 1', 1, 1, CAST(N'2021-04-15' AS Date), NULL, CAST(N'0001-01-01' AS Date), NULL)
INSERT [dbo].[Book] ([Id], [Editorial], [Name], [Status], [AuthorId], [DatePublish], [UpdateAt], [CreateAt], [DeleteAt]) VALUES (3, N'Kiliam', N'name 3', 1, 1, CAST(N'2021-04-08' AS Date), NULL, CAST(N'0001-01-01' AS Date), NULL)
SET IDENTITY_INSERT [dbo].[Book] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([Id], [Firstname], [Lastname], [Password], [Email], [UserName], [SecurityStamp], [EmailConfirmed], [UserEmail], [TwoFactorEnabled], [PhoneNumberConfirmed], [PhoneNumber], [NormalizedEmail], [AccessFailedCount], [ConcurrencyStamp], [LockoutEnabled], [NormalizedUserName], [PasswordHash], [LockoutEnd]) VALUES (1, N'Sergio', N'Orjuela', NULL, N'saor89@gmail.com', N'che', N'3SFZ2KYKF2MTM26IBEEJEYBOYLVFSUPC', 1, N'saor89@gmail.com', 0, 0, NULL, N'SAOR89@GMAIL.COM', 0, N'3286e10e-2121-447f-a9bf-8db1ab082d84', 1, N'1', N'AQAAAAEAACcQAAAAED/7eLmlFWX405kBSxqUlL9qiLddTGHCWWzrpn/8CPj3+Vo+ga1nfD19JGl7Q5a6Pg==', NULL)
INSERT [dbo].[User] ([Id], [Firstname], [Lastname], [Password], [Email], [UserName], [SecurityStamp], [EmailConfirmed], [UserEmail], [TwoFactorEnabled], [PhoneNumberConfirmed], [PhoneNumber], [NormalizedEmail], [AccessFailedCount], [ConcurrencyStamp], [LockoutEnabled], [NormalizedUserName], [PasswordHash], [LockoutEnd]) VALUES (2, N'Sergio', N'Orjuela', NULL, N'saor89@yopmail.com', N'UYR7R', N'D76IFUFCF7JEJM57DKWZBNDNCG6T65ZC', 0, NULL, 0, 0, NULL, N'SAOR89@YOPMAIL.COM', 0, N'eb567c29-ddfd-42b9-a26d-3edf0bce0550', 1, N'UYR7R', N'AQAAAAEAACcQAAAAEN/GGxCqC5a40XSS7Ubwe7zeg4M0KUUjqUU21l6Onc4DC1NcQu0H0zHK/dQKQAmVlg==', NULL)
INSERT [dbo].[User] ([Id], [Firstname], [Lastname], [Password], [Email], [UserName], [SecurityStamp], [EmailConfirmed], [UserEmail], [TwoFactorEnabled], [PhoneNumberConfirmed], [PhoneNumber], [NormalizedEmail], [AccessFailedCount], [ConcurrencyStamp], [LockoutEnabled], [NormalizedUserName], [PasswordHash], [LockoutEnd]) VALUES (3, N'aere', N'testes', NULL, N'saor89@tester.com', N'JFBUV', N'NHXF37BSV4HNRXBL2PLBFW6HSMTBXOCY', 0, NULL, 0, 0, NULL, N'SAOR89@TESTER.COM', 0, N'aa67a4a0-ed7b-4e94-a95f-3ede0bea2410', 1, N'JFBUV', N'AQAAAAEAACcQAAAAEATbxMyRX0X4C8H72zAbfWpjPmib60Kd0eeWSHHbULbrJfNsFykfQN23ucouzF/+fg==', NULL)
SET IDENTITY_INSERT [dbo].[User] OFF
GO
USE [master]
GO
ALTER DATABASE [booksdb] SET  READ_WRITE 
GO
