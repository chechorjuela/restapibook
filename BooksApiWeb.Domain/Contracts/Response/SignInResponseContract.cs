﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksApiWeb.Domain.Contracts.Response
{
    public class SignInResponseContract
    {
        //public UserResponseContract user { get; set; }
        public string token { get; set; }
        public DateTime expireTo { get; set; }
    }
}
