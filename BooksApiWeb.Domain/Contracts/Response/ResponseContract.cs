﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksApiWeb.Domain.Contracts.Request
{
    public class ResponseContract<TContract>
    {
        public HeaderContract Header { get; set; }
        public TContract Data { get; set; }
        public ResponseContract()
        {
            Header = new HeaderContract();
            Header.Code = HttpCodes.Ok;
        }
    }
    public enum HttpCodes
    {
        Ok = 200,
        BadRequest = 400,
        NotFound = 404,
        ValidationError = 421,
        InternalServerError = 500
    }
    public class HeaderContract
    {
        public HttpCodes Code { get; set; }
        public string Message { get; set; }
    }
}
