﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksApiWeb.Domain.Contracts.Response
{
    public class BookResponseContract
    {
        public int Id;
        public string Name;
        public int AuthorId;
        public string Editorial;
        public int Status;
        public DateTime DatePublish;
        public DateTime Create_at;
        public DateTime? Update_at;

    }
}
