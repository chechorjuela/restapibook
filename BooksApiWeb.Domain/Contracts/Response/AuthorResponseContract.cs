﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksApiWeb.Domain.Contracts.Response
{
    public class AuthorResponseContract
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
        public DateTime Birthdate { get; set; }
        public DateTime Create_at { get; set; }
        public DateTime? Update_at { get; set; }
    }
}
