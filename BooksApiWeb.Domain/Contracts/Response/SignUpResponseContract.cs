﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksApiWeb.Domain.Contracts.Response
{
    public class SignUpResponseContract
    {
        public string UserEmail { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string RptPassword { get; set; }
    }
}
