﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksApiWeb.Domain.Contracts.Request
{
    public class AuthorRequestContract
    {
        public string Fullname { get; set; }
        public DateTime Birthdate { get; set; }
    }
}
