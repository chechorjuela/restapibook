﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksApiWeb.Domain.Contracts.Request
{
    public class BookRequestContract
    {
        public int Status { get; set; }
        public string Name { get; set; }
        public int AuthorId { get; set; }
        public DateTime DatePublish { get; set; }
        public string Editorial { get; set; }
    }
}
