﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksApiWeb.Domain.Contracts.Request
{
    public class SignUpRequestContract
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string UserEmail { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
    }
}
