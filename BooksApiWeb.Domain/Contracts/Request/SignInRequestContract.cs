﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksApiWeb.Domain.Contracts
{
    public class SignInRequestContract
    {
        public string userEmail { get; set; }
        public string password { get; set; }
    }
}
