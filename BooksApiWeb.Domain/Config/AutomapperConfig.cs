﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BooksApiWeb.Domain.Contracts.Response;
using BooksApiWeb.Model.Models;

namespace BooksApiWeb.Domain.Config
{
    public class AutomapperConfig
    {
        public static void CreateMaps()
        {
            try
            {
                Mapper.Initialize(cfg =>
                {
                    cfg.CreateMap<Book, BookResponseContract>().ReverseMap();
                });
            }
            catch (Exception exp) { 
            
            }
        }
    }
}
