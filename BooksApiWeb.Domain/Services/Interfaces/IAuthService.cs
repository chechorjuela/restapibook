﻿using BooksApiWeb.Domain.Contracts;
using BooksApiWeb.Domain.Contracts.Request;
using BooksApiWeb.Domain.Contracts.Response;
using System.Threading.Tasks;

namespace BooksApiWeb.Domain.Services
{
    public interface IAuthService
    {
        Task<SignInResponseContract> SignIn(SignInRequestContract SignInrequest);
        Task<SignUpResponseContract> SignUp(SignUpRequestContract SignUprequest);
        SignUpResponseContract forgotPassword();
    }
}
