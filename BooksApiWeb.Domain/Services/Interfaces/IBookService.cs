﻿using BooksApiWeb.Domain.Contracts.Request;
using BooksApiWeb.Domain.Contracts.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksApiWeb.Domain.Services.Interfaces
{
    public interface IBookService
    {
        List<BookResponseContract> getAllBook();
        Task<BookResponseContract> findById(int id);
        Task<BookResponseContract> create(BookRequestContract book);
        Task<BookResponseContract> update(int id,BookRequestContract book);
        Task<Boolean> delete(int book);

    }
}
