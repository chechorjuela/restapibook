﻿using BooksApiWeb.Domain.Contracts.Request;
using BooksApiWeb.Domain.Contracts.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksApiWeb.Domain.Services
{
    public interface IAuthorService
    {
        List<AuthorResponseContract> getAll();
        Task<AuthorResponseContract> findById(int id);
        Task<AuthorResponseContract> create(AuthorRequestContract author);
        Task<AuthorResponseContract> update(int id, AuthorRequestContract author);
        Task<Boolean> delete(int id);
    }
}
