﻿using AutoMapper;
using BooksApiWeb.Domain.Contracts.Request;
using BooksApiWeb.Domain.Contracts.Response;
using BooksApiWeb.Domain.Services.Interfaces;
using BooksApiWeb.Model.Models;
using BooksApiWeb.Repository.Core.Interfaces;
using BooksApiWeb.Repository.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksApiWeb.Domain.Services
{
    public class BookService : IBookService
    {
        private readonly IBookRepository _bookRepository;
        public BookService(IBookRepository bookRepository) {
            this._bookRepository = bookRepository;
        }
        public async Task<BookResponseContract> create(BookRequestContract book)
        {
            Book bookSave = Mapper.DynamicMap<Book>(book);
            bookSave.CreateAt = new DateTime();
            var bookResponse = _bookRepository.Add(bookSave);
            return Mapper.DynamicMap<BookResponseContract>(bookResponse);
        }

        public async Task<bool> delete(int id)
        {
            var response = false;
            var bookDelete = _bookRepository.Get(id);
            var consult = _bookRepository.Remove(bookDelete);
            if (consult!=null) {
                response = true;
            }
            return response;
        }

        public async Task<BookResponseContract> findById(int id)
        {
            var bookGet = _bookRepository.Get(id);
            return Mapper.DynamicMap<BookResponseContract>(bookGet);
        }

        public List<BookResponseContract> getAllBook()
        {
            List<BookResponseContract> listBooks = new List<BookResponseContract>();
            List<Book> listBook = this._bookRepository.GetAll();
            if (listBook!=null)
            {
                listBooks = Mapper.DynamicMap<List<BookResponseContract>>(listBook);
            }
            return listBooks;
        }

        public async Task<BookResponseContract> update(int id,BookRequestContract book)
        {
            Book bookSave = Mapper.DynamicMap<Book>(book);
            bookSave.Id = id;
            var bookGet = _bookRepository.UpdateBook(id, bookSave);
            BookResponseContract bookRepo = Mapper.DynamicMap<BookResponseContract>(bookGet);
            return bookRepo;
        }
    }
}
