﻿using BooksApiWeb.Domain.Contracts.Request;
using BooksApiWeb.Domain.Contracts.Response;
using BooksApiWeb.Repository.Config;
using BooksApiWeb.Repository.Core;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using BooksApiWeb.Domain.Contracts;
using BooksApiWeb.Model.Models;

namespace BooksApiWeb.Domain.Services
{
    public class AuthService : IAuthService
    {
        private readonly IUserRepository _IUserRepository;
        private readonly UserManager<User> _userManager;
        private readonly IConfiguration _configuration;
        public AuthService(IUserRepository IuserRepository,UserManager<User> userManager,IConfiguration configuration) {
            _IUserRepository = IuserRepository;
            _configuration = configuration;
            _userManager = userManager;
        }
        public SignUpResponseContract forgotPassword()
        {
            throw new NotImplementedException();
        }

        public async Task<SignInResponseContract> SignIn(SignInRequestContract SignInrequest)
        {
            SignInResponseContract signResponse = new SignInResponseContract();
            var userModel = await _userManager.FindByEmailAsync(SignInrequest.userEmail);
            if (userModel!=null && await _userManager.CheckPasswordAsync(userModel, SignInrequest.password)) {

                var authSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("ijurkbdlhmklqacwqzdxmkkhvqowlyqa"));
                
                var token = new JwtSecurityToken(
                  expires: DateTime.Now.AddDays(10),
                  claims: null,
                  signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                  );
                signResponse.token = new JwtSecurityTokenHandler().WriteToken(token);
                signResponse.expireTo = token.ValidTo;
            }
            return signResponse;

        }
        private static Random random = new Random();
        public async Task<SignUpResponseContract> SignUp(SignUpRequestContract SignUprequest)
        {
            SignUpResponseContract response = new SignUpResponseContract();
            var user = await _userManager.FindByEmailAsync(SignUprequest.UserEmail);
            if (user==null) {
                User appUser = new User()
                {
                    SecurityStamp = Guid.NewGuid().ToString(),
                    FirstName = SignUprequest.Firstname,
                    LastName = SignUprequest.Lastname,
                    Email = SignUprequest.UserEmail,

                };
                const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

                appUser.UserName = new string(Enumerable.Repeat(chars, 5)
                  .Select(s => s[random.Next(s.Length)]).ToArray());
                var result = await _userManager.CreateAsync(appUser, SignUprequest.Password);

            }
            return response;
        }
    }
}
