﻿using AutoMapper;
using BooksApiWeb.Domain.Contracts;
using BooksApiWeb.Domain.Contracts.Request;
using BooksApiWeb.Domain.Contracts.Response;
using BooksApiWeb.Model.Models;
using BooksApiWeb.Repository.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksApiWeb.Domain.Services
{
    public class AuthorService : IAuthorService
    {
        private readonly IAuthorRepository _authorRepository;
        public AuthorService(IAuthorRepository authorRepository) {
            _authorRepository = authorRepository;
        }
        public async Task<AuthorResponseContract> create(AuthorRequestContract book)
        {
            Author authorSave = Mapper.DynamicMap<Author>(book);
            authorSave.Create_at = new DateTime();
            var authorResponse = _authorRepository.Add(authorSave);
            return Mapper.DynamicMap<AuthorResponseContract>(authorResponse);
        }

        public async Task<bool> delete(int id)
        {
            var response = false;
            var authorDelete = _authorRepository.Get(id);
            var consult = _authorRepository.Remove(authorDelete);
            if (consult != null)
            {
                response = true;
            }
            return response;
        }

        public async Task<AuthorResponseContract> findById(int id)
        {
            var authorDelete = _authorRepository.Get(id);
            return Mapper.DynamicMap<AuthorResponseContract>(authorDelete);
        }

        public List<AuthorResponseContract> getAll()
        {
            List<AuthorResponseContract> listAuthor = new List<AuthorResponseContract>();
            List<Author> listAuthors = _authorRepository.GetAll();
            if (listAuthors != null)
            {
                listAuthor = Mapper.DynamicMap<List<AuthorResponseContract>>(listAuthors);
            }
            return listAuthor;
        }

        public async Task<AuthorResponseContract> update(int id, AuthorRequestContract author)
        {
            Author authorSave = Mapper.DynamicMap<Author>(author);
            authorSave.Id = id;
            authorSave.Update_at =  DateTime.Now;
            var authorGet = _authorRepository.UpdateAuthor(id, authorSave);
            AuthorResponseContract authorRepo = Mapper.DynamicMap<AuthorResponseContract>(authorGet);
            return authorRepo;

           
        }
    }
}
